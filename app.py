from flask import Flask, render_template, request
import os
from nfp.image import build_image_data
from nfp.data_access import get_most_recent_unpatrolled_images

app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))



@app.route("/")
def hello():
    return render_template('home.html')


@app.route("/queue", methods=['GET'])
def schedule():
    return render_template('queue.html')


@app.route("/queue/<wiki>")
def schedule_post(wiki):
    if wiki not in ['commons']:
        return render_template('queue.html')
    from_rc_id = request.args.get('from')
    if from_rc_id:
        try:
            from_rc_id = int(from_rc_id)
        except:
            from_rc_id = None

    if os.getenv('FLASK_ENV') == 'development':
        images = [
            {'title': 'Gustaf Tisell SPA7.jpg', 'user': 'Salgo60','rcid': 1000},
            {'title': 'Saltwater Crocodiles of Sundarbans 07.jpg', 'user': 'Fabian Roudra Baroi', 'rcid': 1001},
        ]
        next_value = 123
    else:
        images, next_value = get_most_recent_unpatrolled_images('commonswiki', from_rc_id)


    return render_template(
        'queue_wiki.html',
        images=build_image_data(images),
        next_value=next_value)



if __name__ == "__main__":
    app.run(host='0.0.0.0')
